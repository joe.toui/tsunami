import pickle
import os
from save_object import saveobject


class parent(saveobject):
	def __init__(self, name,kide_name_select):
		self.name = name
		self.kide_name_select = kide_name_select
		self.kids = []
		super().__init__()

	def add_kide(self, kide_name):
		self.kids.append(kide_name)

	def save_obg(self):
		print(str(type(self)).split("'")[1].split('.')[1])
		directrory = 'data/' + str(type(self)).split("'")[1].split('.')[1]
		if not os.path.exists(directrory):
			os.makedirs(directrory)
		filehandler = open(directrory + '/'+self.name + '.obj', "wb")
		pickle.dump(self,filehandler)
		filehandler.close()

