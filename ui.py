from flask import Flask, render_template, request
from flask_nav.elements import *
from clubedate import clubedate
from payment import payment
from kide import kide
from parents import parent
from pathlib import Path
import pickle
import os
import glob
from pathlib import Path
obg_dict = {}

app = Flask(__name__)



@app.route('/')
def index():
    return render_template('index.html')

@app.route('/add_kide', methods=['POST',"GET"])
def add_kide():
    if request.method == 'POST':
        kide_birthday = request.form['birthday']
        kide_name = request.form['kide_name']
        new_kide = kide(kide_name, kide_birthday )
        new_kide.date_of_berth = kide_birthday
        new_kide.save_obg()
        obg_dict['kide'].append(new_kide)
    kide_list = {}
    try:
        for record in obg_dict['kide']:
            print(record)
            kide_list[record] = obg_dict['kide'][record]
    except:
        os.mkdir('data/kide')
        obg_dict['kide'] = {}
        return add_kide()
    return render_template('add_kide.html', KIDE=kide_list)



@app.route('/show_clubedate', methods=['POST',"GET"])
def show_clubedate():
    clubedatelist = []
    for clubedate in obg_dict['clubedate']:
        clubedatelist.append(clubedate)
    return render_template('show_clubedate.html', clubedatelist=clubedatelist)


@app.route('/show_kids_payments', methods=['POST', "GET"])
def show_kids_payments():
    payment_list = {}
    if request.method == 'POST':
        kide_name = request.form['kide_name_show']
        for payment in obg_dict['payment']:
            if obg_dict['payment'][payment].kide == kide_name:
                payment_list[payment] = obg_dict['payment'][payment]
    else:
        for payment in obg_dict['payment']:
            payment_list[payment] = obg_dict['payment'][payment]
    return render_template('show_payment_of_kide.html', payment_list=payment_list)


@app.route('/add_payment', methods=['POST', "GET"])
def add_payment():
    payment_list = {}
    for pay in obg_dict['payment']:
        payment_list[obg_dict['payment'][pay].name] = obg_dict['payment'][pay]
    if request.method == 'POST':
        kide_name = request.form['kide_name_select']
        payment_data = request.form['payment']
        this_payment = payment(kide_name, payment_data)
        this_payment.save_obg()
        obg_dict['payment'][this_payment.name] = this_payment
    kide_list = {}
    for record in obg_dict['kide']:
        kide_list[record] = obg_dict['kide'][record]
    return render_template('add_payment.html', KIDE=kide_list, payment_list=payment_list)


@app.route('/add_clubedate', methods=['POST',"GET"])
def add_clubedate():
    #clubedate = request.form['birthday']
    if request.method == 'POST':
        clubed = clubedate(request.form['add_clubedate'])
        clubed.save_obg()
        obg_dict['clubedate'][clubed.name] = clubed
    clubedate_list = {}
    try:
        for record in obg_dict['clubedate']:
            print(record)
            clubedate_list[obg_dict['clubedate'][record].name]=obg_dict['clubedate'][record]
    except:
        obg_dict['clubedate'] = {}
        return add_clubedate()
    return render_template('add_clubedate.html', CLUBDATE=clubedate_list)

@app.route('/add_parent', methods=['POST',"GET"])
def add_parent():
    kide_list = []
    for record in obg_dict['kide']:
        kide_list.append(record)
    if request.method == 'POST':
        new_parent = parent(request.form['parent_name'],request.form['kide_name_select'])
        new_parent.save_obg()
        print(obg_dict['kide'].keys)
    return render_template('add_parent.html', kide_list=kide_list)

@app.route('/clube_date_manage', methods=['POST',"GET"])
def clube_date_manage():
    if request.method == 'POST':
        return render_template('clube_date_manage.html')


def calculete_payment():
    pass


if __name__ == "__main__":
    print('object_class')
    Path('data/kide').mkdir(exist_ok=True)
    Path('data/payment').mkdir(exist_ok=True)
    Path('data/clubedate').mkdir(exist_ok=True)
    for object_class in glob.glob("data/*"):
        object_class = object_class.split('/')[-1]
        print('object_class')
        print(object_class)
        obg_dict[object_class] = {}
        for path in Path('data/' + object_class).rglob('*.obj'):
            obj_file = open(path, 'rb')
            tmp_obj = pickle.load(obj_file)
            obg_dict[object_class][tmp_obj.name] = tmp_obj
            print(obj_file)
            obj_file.close()
    app.run(debug=True, port=1948)